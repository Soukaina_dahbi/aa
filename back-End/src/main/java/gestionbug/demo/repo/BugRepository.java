package gestionbug.demo.repo;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import gestionbug.demo.domain.Bug;

@CrossOrigin
@RepositoryRestResource(collectionResourceRel = "bugs", path = "bugs")
public interface BugRepository extends JpaRepository<Bug, Long> {

	List<Bug> findAllByType(String type);

	List<Bug> findByDate(Date date);

	List<Bug> findByStatut(String statut);

	Bug findByIdB(Long id);

}
