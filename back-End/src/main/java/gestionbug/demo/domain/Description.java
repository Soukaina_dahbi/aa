package gestionbug.demo.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Description {

	@Id
	@GeneratedValue
	private Long idD;
	private String message;
	@ManyToOne
	private Bug bug;

	public Description() {
	}

	public Description(Long idD, String message, Bug bug) {
		super();
		this.idD = idD;
		this.message = message;
		this.bug = bug;
	}

	public Long getIdD() {
		return idD;
	}

	public void setIdD(long idD) {
		this.idD = idD;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Bug getBug() {
		return bug;
	}

	public void setBug(Bug bug) {
		this.bug = bug;
	}

	public void setIdD(Long idD) {
		this.idD = idD;
	}

}
