package gestionbug.demo.rest;

import gestionbug.demo.domain.Projet;
import gestionbug.demo.service.ProjetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("projet")
public class ProjetController {
    @Autowired
    private ProjetService projetService;

    @PostMapping("")
    public Projet add(@RequestBody Projet projet)
    {
        return projetService.add(projet);
    }

    @GetMapping("/find/{id}")
    public Projet find(@PathVariable Long id){return  projetService.find(id);}

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { projetService.delete(id);}

    @PutMapping("")
    public Projet edite(@RequestBody Projet projet){ return projetService.edit(projet);}


    @GetMapping("/search")
    public List<Projet> find(@RequestParam String nom){
        return projetService.search(nom);
    }



}
