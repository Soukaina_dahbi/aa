package gestionbug.demo.rest;
import gestionbug.demo.domain.Description;
import gestionbug.demo.service.DescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("description")
public class DescriptionControlleur {
@Autowired
    DescriptionService descriptionService;
    @GetMapping("")
    public List<Description> getAll() {
        return descriptionService.getAll();}

    @PostMapping("")
    public ResponseEntity<?> add(@RequestBody Description description )
    {
        return descriptionService.add(description);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) { descriptionService.delete(id);}

    @PutMapping("")
    public  Description edite(@RequestBody Description description ){ return descriptionService.edit(description);}
}
