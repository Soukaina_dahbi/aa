import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BugListResponse } from './BugListResponse';

@Injectable({
  providedIn: 'root'
})
export class BugService {

  private baseUrl = 'http://localhost:8080/api/bugs';

  constructor(private http: HttpClient) { }

  getBug(id: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${id}`);
  }

  createBug(bug: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}`, bug);
  }

  updateBug(id: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${id}`, value);
  }

  deleteBug(id: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${id}`, { responseType: 'text' });
  }

  getBugsList() {
    return this.http.get<BugListResponse>(`${this.baseUrl}`);
  }
}