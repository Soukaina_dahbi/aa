import { Bug } from './bug';

export interface BugListResponse {
    page: {
        size: number,
        totalElements: number,
        totalPages: number,
        number: number
    };
    _embedded: {
        bugs: Bug[];
    };
}