import { Component, OnInit } from '@angular/core';
import { Observable } from "rxjs";
import { BugService } from "./../bug.service";
import { Bug } from "./../bug";

@Component({
  selector: 'app-bug-list',
  templateUrl: './bug-list.component.html',
  styleUrls: ['./bug-list.component.css']
})
export class BugListComponent implements OnInit {

  bugs: any;

  constructor(private bugService: BugService) {}

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.bugService.getBugsList().subscribe(res => {
      this.bugs = res._embedded.bugs;
    }); 
  }

}
