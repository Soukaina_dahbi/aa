import { Component, OnInit } from '@angular/core';
import { BugService } from "./../bug.service";
import { Bug } from "./../bug";

@Component({
  selector: 'app-create-bug',
  templateUrl: './create-bug.component.html',
  styleUrls: ['./create-bug.component.css']
})
export class CreateBugComponent implements OnInit {

  bug: Bug = new Bug();
  submitted = false;

  constructor(private bugService: BugService) { }

  ngOnInit() {
  }

  newBug(): void {
    this.submitted = false;
    this.bug = new Bug();
  }

  save() {
    this.bugService.createBug(this.bug)
      .subscribe(data => console.log(data), error => console.log(error));
    this.bug = new Bug();
  }

  onSubmit() {
    this.submitted = true;
    this.save();
  }

}
